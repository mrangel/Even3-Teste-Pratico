using System;
using System.Threading.Tasks;
using jsreport.Binary;
using jsreport.Local;
using jsreport.Types;
using System.IO;

namespace GeralPdf
{
    class CriarPdf
    {
        private string conteudo {get; set; }
        private string caminho {get; set; }

        public CriarPdf( string conteudo, string caminho ){
            this.conteudo = conteudo;
            this.caminho = caminho;
            RenderizarPdf().Wait();
        }
        
        private async Task RenderizarPdf(){

            var rs =  new LocalReporting().UseBinary(JsReportBinary.GetBinary()).AsUtility().Create();

            var modelo = await rs.RenderAsync( new RenderRequest
            {
                Template = new Template
                {
                    Recipe = Recipe.PhantomPdf,
                    Engine = Engine.None,
                    Content = this.conteudo
                }
            });
            
            using (var salvarArq = File.Create(this.caminho))
            {
                modelo.Content.CopyTo(salvarArq);
            }
        }

    }
}