using Microsoft.EntityFrameworkCore;

namespace entity 
{
    /// <summary>
    /// This class handles the sqlite database
    /// </summary>
    public class UserListContext : DbContext
    {
        /// <summary>
        /// This property allows to manipoulate the databasetable
        /// </summary>
        public DbSet<UserList> UserList {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            // Specify the path of the database here
            optionsBuilder.UseSqlite("Filename=./userlist.sqlite");
        }
    }
}