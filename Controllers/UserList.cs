using System.ComponentModel.DataAnnotations;

namespace entity {
    public class UserList {
        [Key]
        public string Registro {get; set;}
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Pais { get; set; }
        public string Instiuicao { get; set; }
        public string Inscricao { get; set; }

    }
}