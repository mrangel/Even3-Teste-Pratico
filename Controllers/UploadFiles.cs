using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace GeralUpload {
    public class UploadImage : Controller {

        [HttpPost ("Upload")]
        public async Task<IActionResult> Post (List<Microsoft.AspNetCore.Http.IFormFile> files) {

            long size = files.Sum (f => f.Length);

            string filesPath = Path.GetTempFileName();


            foreach (var formFile in files) {
                if (formFile.Length > 0) {
                    using (var stream = new FileStream (filesPath, FileMode.Create)) {
                        await formFile.CopyToAsync (stream);
                    }
                }
            }
            return View();
        }
    }
}